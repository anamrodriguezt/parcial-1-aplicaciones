<!doctype html>
<html lang="es">
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script
	src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<title>Amazonas</title>
<link rel="icon" type="image/png" href="img/logo.png">
</head>
<body>
	<div class="container">
		<div class="row mt-3">
			<div class="col-3 text-center">
				<img src="img/logo.png" width="100px">
			</div>
			<div class="col-9 text-center">
				<h1>Amazonas</h1>
			</div>
		</div>

		<div class="row mt-3">
			<div class="col-md">
				<div class="card">
					<div class="card-header">
						<h3>Proyecto Amazonas</h3>
					</div>
					<div class="card-body">
						<img src="img/img1.jpg" width="100%">
					</div>
				</div>
			</div>
			<div class="col-4">
				<div class="card">
					<div class="card-header">
						<h3>Autenticación</h3>
					</div>
					<div class="card-body">
						<form>
							<div class="form-group">
								<input type="email" class="form-control" placeholder="Correo"
									required="required">
							</div>
							<div class="form-group">
								<input type="password" class="form-control" placeholder="Clave"
									required="required">
							</div>
							<div class="form-group">
								<button type="submit" class="btn btn-primary">Submit</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
